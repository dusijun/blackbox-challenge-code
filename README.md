# Kaggle Blackbox Learning Challenge   
This repo hosts code for the winning entry in the Kaggle Blackbox Learning Challenge.
Briefly, it uses sparse filtering, feature selection, aggregation of selected features across several feature sets,
and svms. Most entries are a single classifier, most are linear kernel. The winning entry was a small ensemble.

## Information:   
Sparse filtering was developed by Ngiam, et. al, at Stanford. Their paper is here:
[sparse filtering paper](http://cs.stanford.edu/~jngiam/papers/NgiamKohChenBhaskarNg2011.pdf)   

The original code is here:[sparse filter code](https://github.com/jngiam/sparseFiltering)   

The optimization solver used there(minFunc package) is better than the one used here,
 so for your other explorations, you should probably get his code.   

My code is explained in a post on the Kaggle forums:
[post](https://www.kaggle.com/c/challenges-in-representation-learning-the-black-box-learning-challenge/forums)

## Contents:   
- Most of my code is in code/
- Ngiam's sparse filter code is in sparse_filtering/
- R code for feature ranking is in R/
- A different repository [artifacts](https://bitbucket.org/dthal/blackbox-challenge-artifacts) has large precomputed weight matrices.

## Dependencies:   
- Get the data: [Data](https://www.kaggle.com/c/challenges-in-representation-learning-the-black-box-learning-challenge/data). 
You will need the unlabeled data as well as the train and test sets.
One of the data-wrangling script expects the csv form of the unlabeled data.
- R with the foreign and randomForest packages
- I used Octave. If you are using Matlab, 
you will probably need to change parts of the read1ocatve.R and write1ocatave.R files.
- libsvm, including the Octave/Matlab interface
There are a few modifications to Ngiam's SF code, so use the code provided here to run this code.

## Set-up/data-wrangling:   
My setup uses the three directories here, plus an output/ directory, initially empty,
and a data/ directory with an unlabeled/ subdirectory. With the csv files from Kaggle at the
top level of data/ running `processData` will leave the training and test sets in the form
in which they are used in the code, as three separate Octave binary files.

The unlabeled data is large, and I had to break it up into pieces to use it. 
With the csv file of the unlabeled data in data/unlabeled/, running the batchup.py 
script breaks the data up into the 5000 row chunks expected in the script for running
sparseFiltering on the large data. Then, in Octave, running bin_batch.m will convert
the csv files into Octave binary files, which load much faster. Unfortunately, that
step (which happens once) could take hours.

You'll need to edit start.m to get the path to libsvm's matlab interace on the path.

## Usage:   
Roughly speaking, there are two main ways to run this code: using precomputed weights 
from the artifacts repositor(see below) or computing a new batch of weights and features and doing it all from scratch.

### Pre-computed weights
call `start` if you haven't already to load the labels, test data and training set, 
and to get libsvm on the path. Then bring in the weights:   
`load artifacts/<some weights>.mat`   
To get a linear kernel svm model from labels, training data, 
some weights, and a range of C values to be tried for the svm, call:   
`model = modelFromWts(y, xtrain, <some weights>, <range of C values>);`   
That call runs a search for the best C value (in 10-fold cv) over the range provided, 
and then trains a model with it. You can get an rbf kernel, but the call is a little different.   
`model = modelFromWts(y, xtrain, <some weights>, <range of g values>, "rbf", C_value);`   
Note that you have to specify a specific c value here while searching for a g value.   
`getPredictions()` makes a prediction from a model, weights, and test data. It returns the predictions 
and also writes them to output/<submission name>.txt in the format used in this competition.   
`predictions = getPredictions(<some weights>, model, xtest, "<submission name>");`   

### From scratch
Working from scratch, we start in Octave by making a bunch of weights and features. The function 
`bunchOfWts()` grabs 5000-row batches of unlabeled data, trains a sparse filter on them for a 
number of iterations, then gets another batch and so on. It does that for each of several sets.
We have to specify a vector of sizes for the feature sets, as well as the number of batches 
to use on each and the iteration count to use each batch for. Something like:   
`sets = bunchOfWts(100*ones(1,10), 54, 20);`   
is about right for cutting the lowest-performing 60-70% of features and ending up at 300-400 
features. The features are written at r_dir, which defaults to ./R. Next we go to R to get ranks 
for feature selection. The call:
`rankAll(yfactor, 10)`   
takes labels as a factor, the number of feature sets to look for (10 in this example),
the number of trees to use (default 1000), and an output directory to write the ranks back to, which
(defaults to output/). It returns ranks in R and writes them in files in output/. At this point, 
I would call appendRanks:   
`sets = appendRanks(sets);`  
which attaches the ranks to the weights and features, so they don't get lost,
and then I would save it using `save -binary artifacts/sets.mat sets`, since all of these artifacts 
take a while to generate.
To get a linear svm model from this bunch of weights and ranks call:   
`[wts model] = buildModel(y, xtrain, sets, <% features kept>, <range of C>);`   
That trains a model with the given percentage of features kept (30-45% works well), and with
a value for the C parameter of libsvm chosen by 10-fold cross-validation at each 
of the values in the specified range of c values. Finally, we generate predictions from weights, a model, test data and a submission file 
name in the same way as above:   
`predictions = getPredictions(<some weights>, model, xtest, "<submission name>");`   
The submission file lands in output/ in the competition format.

## Artifacts:   
Pre-computed weights are in another repository at: [artifacts](https://bitbucket.org/dthal/blackbox-challenge-artifacts)   
These were used in the high-scoring entry, and wts300 by itself was used for an entry that got 0.7018,
which seems to be 2 back on the 5000 exaple test set.   
  
## High-scoring entry:   
Strictly speaking, the winning entry was a small, voted ensemble of other high-scoring models. 
It was 2 predictions better, out of 5000, than the best single-classifier entry. 
It wasn't the high-scoring entry on the public leaderboard. I didn't write a code to make it.
I didn't follow up on it. I don't think it is worth bothering with. 
If you want to reproduce it anyway, here's how:   
Get the contents of the [artifacts](https://bitbucket.org/dthal/blackbox-challenge-artifacts) repo.   
In Octave:   
call `start` if you haven't already to load the labels, test data and training set   

    load artifacts/wts274.mat  
    load artifacts/wts300.mat  
    load artifacts/wts400.mat  
    m1 = modelFromWts(y, xtr, wts400, 1, "rbf");  
    m2 = modelFromWts(y, xtr, wts300, 9);  
    m3 = modelFromWts(y, xtr, wts274, 0.4, "nu");  
    p1 = getPredictions(wts400, m1, xtest, "dummy");  
    p2 = getPredictions(wts300, m2, xtest, "dummy");  
    p3 = getPredictions(wts274, m3, xtest, "dummy");  
    pred = mode([p1 p2 p3], 2);   

...and that should get 0.7022 on the private leaderboard.
